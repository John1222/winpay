<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../src/TinyHTTPClient.php';

use brocoder\WinPay\TinyHTTPClient\TinyHTTPClient;
use brocoder\WinPay\TinyHTTPClient\URLContentFormatter;

$httpClient = new TinyHTTPClient( 'https://www.google.com/' );
$httpClient->setupURLContentFormatters( [
        new URLContentFormatter(
            '/<img alt="Google".*?>/',
            '<img width="272" alt="Google" src="https://cs8.pikabu.ru/post_img/2017/09/01/9/1504281297196980206.png" style="padding:28px 0 14px" id="hplogo" onload="window.lol&amp;&amp;lol()">'
        ),
        new URLContentFormatter(
            '/<input class="lsb".*?>/',
            '<input value="Мы все умрем" name="btnG" class="lsb" type="submit">'
        )
    ]
);
echo $httpClient->getUrlContent();