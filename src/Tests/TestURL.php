<?php
namespace brocoder\WinPay\TinyHTTPClient\Tests;

class TestURL
{
    /**
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $content;

    public function __construct( string $url, string $content )
    {
        $this->url = $url;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}