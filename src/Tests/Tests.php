<?php
namespace brocoder\WinPay\TinyHTTPClient\Tests;

require_once __DIR__ . '/../TinyHTTPClient.php';
require_once __DIR__ . '/TestURLs.php';

use brocoder\WinPay\TinyHTTPClient\LoopingProtectionException;
use brocoder\WinPay\TinyHTTPClient\TinyHTTPClient;
use brocoder\WinPay\TinyHTTPClient\URLContentFormatter;
use brocoder\WinPay\TinyHTTPClient\URLNotSpecifiedException;
use PHPUnit\Framework\TestCase;

class Tests extends TestCase
{
    public static function setUpBeforeClass()
    {
        TinyHTTPClient::clearLogs();
    }

    /**
     * При попытке вызвать URLContentFormatter с параметрами, подверженных зацикливанию, будет выброшено исключение.
     */
    public function testLoopingProtection()
    {
        $exceptionExpectedSets = [
            [ '/text/', 'simple text mega' ],
            [ '/text.*/', 'simple text' ],
            [ '/te.*?xt/', 'simple text little' ],
            [ '/.*simple.*/', 'very simple text' ]
        ];
        foreach( $exceptionExpectedSets as $exceptionExpectedSet ) {
            $isTestPassed = false;
            try {
                new URLContentFormatter( $exceptionExpectedSet[ 0 ], $exceptionExpectedSet[ 1 ], true );
            }
            catch( LoopingProtectionException $e ) {
                $isTestPassed = true;
            }
            $this->assertTrue( $isTestPassed, 'Exception expected, but don\'t threw' );
        }

        $exceptionNotExpectedSets = [
            [ '/test/', 'simple text mega' ],
            [ '/simplest/', 'simple text mega' ],
            [ '/nigger/', 'dirty nigga goes to hell' ],
            [ '/whore/', 'she loves big cocks' ]
        ];
        foreach( $exceptionNotExpectedSets as $exceptionNotExpectedSet ) {
            $this->assertInstanceOf(
                URLContentFormatter::class,
                new URLContentFormatter( $exceptionNotExpectedSet[ 0 ], $exceptionNotExpectedSet[ 1 ] )
            );
        }
    }

    /**
     * Тестим сразу два момента:
     * 1) setupContentFormatters
     * 2) отсутствие рекурсивной замены
     */
    public function testNonRecursiveFormatting()
    {
        $url = new TinyHTTPClient( TestURLs::URL_CONTENT_FORMATTING()->getUrl() );
        $url->setupURLContentFormatters( [
            new URLContentFormatter( '/google/', '' ),
            new URLContentFormatter( '/facebook/', '' )
        ] );
        $this->assertEquals( 'google', $url->getUrlContent() );
    }

    /**
     * Тестим сразу два момента:
     * 1) setupContentFormatter
     * 2) присутствие рекурсивной замены
     */
    public function testRecursiveFormatting()
    {
        $url = new TinyHTTPClient( TestURLs::URL_CONTENT_FORMATTING()->getUrl() );
        $url->setupURLContentFormatter( new URLContentFormatter( '/google/', '', true ) );
        $this->assertFalse( strpos( $url->getUrlContent(), 'google' ) );
    }

    /**
     * URL задан в конструкторе и является урлом по умолчанию, если только при вызове метода явно не указан другой.
     */
    public function testURLInitializationInConstructor()
    {
        // урл задан при инициализации
        $url = new TinyHTTPClient( TestURLs::URL_CONTENT_1()->getUrl() );
        // должно запросить контент с урла, указанного в конструкторе
        $this->assertEquals( TestURLs::URL_CONTENT_1()->getContent(), $url->getUrlContent() );
        // должно запросить контент с урла, переданного в getUrlContent
        $this->assertEquals(
            TestURLs::URL_CONTENT_2()->getContent(),
            $url->getUrlContent( TestURLs::URL_CONTENT_2()->getUrl() )
        );
    }

    /**
     * URL не задан в конструкторе, но указан при вызове метода.
     */
    public function testURLInitializationDuringGettingContent()
    {
        $url = new TinyHTTPClient;
        $this->assertEquals(
            TestURLs::URL_CONTENT_1()->getContent(),
            $url->getUrlContent( TestURLs::URL_CONTENT_1()->getUrl() )
        );
    }

    /**
     * Если URL не задан в конструкторе и не указан при вызове метода, то должно быть исключение.
     */
    public function testURLNotSpecified()
    {
        $url = new TinyHTTPClient;
        $this->expectException( URLNotSpecifiedException::class );
        $url->getUrlContent();
    }

    public function testGettingContentFromHTTP()
    {
        $url = new TinyHTTPClient( TestURLs::URL_HTTP()->getUrl() );
        $this->assertRegExp( TestURLs::URL_HTTP()->getContent(), $url->getUrlContent() );
    }

    public function testGettingContentFromHTTPS()
    {
        $url = new TinyHTTPClient( TestURLs::URL_HTTPS()->getUrl() );
        $this->assertRegExp( TestURLs::URL_HTTPS()->getContent(), $url->getUrlContent() );
    }
}