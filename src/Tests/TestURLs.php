<?php
namespace brocoder\WinPay\TinyHTTPClient\Tests;

require_once __DIR__ . '/TestURL.php';

class TestURLs
{
    /**
     * @var TestURL
     */
    private static $URL_CONTENT_1;
    /**
     * @var TestURL
     */
    private static $URL_CONTENT_2;
    /**
     * @var TestURL
     */
    private static $URL_CONTENT_REDACTION;
    /**
     * @var TestURL
     */
    private static $URL_HTTP;
    /**
     * @var TestURL
     */
    private static $URL_HTTPS;

    /**
     * @return TestURL
     */
    public static function URL_CONTENT_1()
    {
        if( self::$URL_CONTENT_1 == null ) {
            self::$URL_CONTENT_1 = new TestURL( 'http://kater-club.ru/books/urls/content1.txt', 'content1' );
        }
        return self::$URL_CONTENT_1;
    }

    /**
     * @return TestURL
     */
    public static function URL_CONTENT_2()
    {
        if( self::$URL_CONTENT_2 == null ) {
            self::$URL_CONTENT_2 = new TestURL( 'http://kater-club.ru/books/urls/content2.txt', 'content2' );
        }
        return self::$URL_CONTENT_2;
    }

    /**
     * @return TestURL
     */
    public static function URL_CONTENT_FORMATTING()
    {
        if( self::$URL_CONTENT_REDACTION == null ) {
            self::$URL_CONTENT_REDACTION = new TestURL(
                'http://kater-club.ru/books/urls/content_formatting.txt',
                'googoogleglefacebookgooglefacebook'
            );
        }
        return self::$URL_CONTENT_REDACTION;
    }

    /**
     * @return TestURL
     */
    public static function URL_HTTP()
    {
        if( self::$URL_HTTP == null ) {
            self::$URL_HTTP = new TestURL( 'http://www.google.ru/', '/^<!doctype html>/' );
        }
        return self::$URL_HTTP;
    }

    /**
     * @return TestURL
     */
    public static function URL_HTTPS()
    {
        if( self::$URL_HTTPS == null ) {
            self::$URL_HTTPS = new TestURL( 'https://www.google.ru/', '/^<!doctype html>/' );
        }
        return self::$URL_HTTPS;
    }
}