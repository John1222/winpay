<?php
namespace brocoder\WinPay\TinyHTTPClient;

require_once __DIR__ . '/Exceptions/FatalException.php';

/**
 * Данный модуль призван модифицировать контент, полученный при запросе URL классом TinyHTTPClient
 */
class URLContentFormatter
{
    private $pattern;
    private $replacement;
    private $isRecursive;

    /**
     * @param string $pattern RegEx того же формата, что и у preg_replace
     * @param string $replacement Обычная строка
     * @param bool $isRecursive Будем ли реплейсить контент до тех пор, пока не иссякнут все вхождения?
     * @throws LoopingProtectionException
     */
    public function __construct( string $pattern, string $replacement, bool $isRecursive = false )
    {
        if( $isRecursive && preg_match( $pattern, $replacement ) ) {
            throw new LoopingProtectionException(
                'You are trying to setup recursive formatter with config susceptible for looping'
            );
        }
        
        $this->pattern = $pattern;
        $this->replacement = $replacement;
        $this->isRecursive = $isRecursive;
    }

    public function applyTo( string $content ): string
    {
        $result = $content;
        $count = 1;
        // рекурсив можно врубить и в регулярках, но мне парсинг юзерской регулярки показался еще большим злом
        while( $count != 0 ) {
            $result = preg_replace(
                $this->pattern,
                $this->replacement,
                $result,
                -1,
                $count
            );
            // мини-хак, чтобы оффнуть дальнейшие реплейсы, если не требуется рекурсив
            if( ! $this->isRecursive ) {
                $count = 0;
            }
        }
        return $result;
    }
}