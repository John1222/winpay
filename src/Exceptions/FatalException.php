<?php
namespace brocoder\WinPay\TinyHTTPClient;

require_once __DIR__ . '/URLNotSpecifiedException.php';
require_once __DIR__ . '/LoopingProtectionException.php';
require_once __DIR__ . '/LoggerInitException.php';
require_once __DIR__ . '/LogFileDeletionProblemException.php';

class FatalException extends \Exception {}