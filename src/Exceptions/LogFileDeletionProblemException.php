<?php
namespace brocoder\WinPay\TinyHTTPClient;

require_once __DIR__ . '/FatalException.php';

class LogFileDeletionProblemException extends FatalException {}