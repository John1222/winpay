<?php
namespace brocoder\WinPay\TinyHTTPClient;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Http\Message\ResponseInterface;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/URLContentFormatter.php';

class TinyHTTPClient
{
    private const LOG_PATH = __DIR__ . '/../logs/logs.log';
    private const REQUEST_OPTIONS = [
        'connect_timeout' => 10,
        'read_timeout' => 10
    ];

    /**
     * @var string
     */
    private $urlDefault;
    /**
     * @var Client
     */
    private $client;
    /**
     * @var URLContentFormatter[]
     */
    private $formatters = [];
    /**
     * @var Logger
     */
    private $logger;

    /**
     * Сей URL будет использоваться по умолчанию при вызове getUrlContent, если только при вызове оной не
     * будет явно задан иной URL.
     * 
     * Вообще, все эти задавания урла в двух местах попахивают скрытой логикой. Юзеру надо будет держать в голове,
     * что при пустом вызове getUrlContent будет запрошен урл из конструктора, а разрабу теперь городи тесты,
     * чтобы при отсутствии урла и там, и там юзеру выплевывалось исключение. Я понимаю, что это такая подъебка
     * для интервьюируемого, но в реальном проекте такого лучше избегать. Если предполагается смена урлов, то пусть
     * юзер в добровольно-принудительном порядке каждый раз сам указывает урл при запросе контента.
     *
     * @param string|null $url
     * @throws LoggerInitException
     */
    public function __construct( string $url = null )
    {
        $this->logger = new Logger( 'TinyHTTPClient' );
        try {
            $this->logger->pushHandler( new StreamHandler( self::LOG_PATH ) );
            $this->logger->debug( '================== Logging started ==================' );
        }
        catch( \Exception $e ) {
            throw new LoggerInitException( "Can't init logger: {$e->getMessage()}" );
        }
        $this->urlDefault = $url;
        $this->client = new Client();
    }

    /**
     * @throws LogFileDeletionProblemException
     */
    public static function clearLogs()
    {
        if( ! file_exists( self::LOG_PATH ) ) {
            return;
        }
        if( ! unlink( self::LOG_PATH ) ) {
            throw new LogFileDeletionProblemException( "Can't delete log file: " . self::LOG_PATH );
        }
    }

    /**
     * @param URLContentFormatter[] $formatters
     */
    public function setupURLContentFormatters( array $formatters )
    {
        $this->formatters = $formatters;
        $this->logger->debug( 'Was set up ' . count( $formatters ) . ' URL content formatters' );
    }

    /**
     * @param URLContentFormatter $formatter
     */
    public function setupURLContentFormatter( URLContentFormatter $formatter )
    {
        $this->formatters = [ $formatter ];
        $this->logger->debug( 'Was set up 1 URL content formatter' );
    }

    /**
     * Если урл не передан при вызове, будет использоваться урл, заданный в конструкторе.
     *
     * @param string|null $url
     * @return string|false
     * @throws URLNotSpecifiedException
     */
    public function getUrlContent( string $url = null ): string
    {
        $urlRequested = '';
        /* ну вот не говнокод, а? чему молодежь учите? :) */
        if( is_null( $url ) && ! is_null( $this->urlDefault ) ) {
            $urlRequested = $this->urlDefault;
        }
        else if( ! is_null( $url ) ) {
            $urlRequested = $url;
        }
        else {
            $this->logger->critical( "URL not specified. Can't make the request." );
            throw new URLNotSpecifiedException();
        }

        $bodyContent = $this->sendRequestAndObtainBodyContent( $urlRequested );
        if( $bodyContent !== false ) {
            return $this->applyContentFormattersTo( $bodyContent );
        }
        return false;
    }

    /**
     * @param string $url
     * @return string|false
     */
    private function sendRequestAndObtainBodyContent( string $url ): string
    {
        try {
            $this->logger->debug( "Sending the request to URL {$url}..." );
            $content = $this->client->request( 'GET', $url, self::REQUEST_OPTIONS )->getBody()->getContents();
            $this->logger->debug( 'Request was success! Content length: ' . strlen( $content ) . '.' );
            return $content;
        }
        catch( GuzzleException $e ) {
            $this->logger->error( "GuzzleException: {$e->getMessage()}" );
            return false;
        }
    }

    private function applyContentFormattersTo( string $content ): string
    {
        $result = $content;
        foreach( $this->formatters as $formatter ) {
            $result = $formatter->applyTo( $result );
        }
        return $result;
    }
}